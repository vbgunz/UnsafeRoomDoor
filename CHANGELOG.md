# UnsafeRoomDoor Change Log
## [0.0.2] - 2018-10-01
### Changed
- Only non-ghost SI can toggle doors
- The distance an SI will need to be to toggle a door

### Added
- A small timeout between door toggles

## [0.0.1] - 2017-04-09
### Added
- First Release

## This CHANGELOG follows http://keepachangelog.com/en/0.3.0/
### CHANGELOG legend

- Added: for new features.
- Changed: for changes in existing functionality.
- Deprecated: for once-stable features removed in upcoming releases.
- Removed: for deprecated features removed in this release.
- Fixed: for any bug fixes.
- Security: to invite users to upgrade in case of vulnerabilities.
- [YANKED]: a tag too signify a release to be avoided.
